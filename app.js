const balanceElement = document.getElementById("balance");
const takeLoanElement = document.getElementById("loanButton");
const currentLoanElement = document.getElementById("currentLoan");
const computersElement = document.getElementById("listedComputers");
const listedComputersElement = document.getElementById("listedComputers");
const featureElement = document.getElementById("feature"); 
const computerImgElement = document.getElementById("compImage"); 
const computerTitleElement = document.getElementById("computerTitle"); 
const computerInfoElement = document.getElementById("computerInfo"); 
const computerPriceElement = document.getElementById("computerPrice"); 
const salaryBalElement = document.getElementById('salaryBalance');
const salaryButtonElement = document.getElementById('salaryButton');
const payBackElement = document.getElementById('payback');
const bankingElement = document.getElementById('banking');
const buyBtnElement = document.getElementById('buyButton');

// Variables with values. *Some from innerHTML.
let loanAmount = 0;
let computers = [];
let balance = document.getElementById("balance").innerHTML;
let laptopPrice = document.getElementById("computerPrice").innerHTML;
let salary = 0;

// "Banker" //
const takeLoan = () => {
    // Make sure there isnt any loan blocking
    if (loanAmount <= 0) {
        let loan = prompt("Please enter the amount of money you wish to loan: ");
        if (loan <= balance * 2) {
            loanAmount = Math.floor(loan);
            alert(`Loan taken: ${loanAmount}`);
            // DOM - Add the loan frontend
            currentLoanElement.innerHTML = `${loanAmount}`;
            // Store current balance with loan
            balance = parseInt(loanAmount) + parseInt(balance);
            // Show total balance frontend
            balanceElement.innerHTML = balance;
            paybackBtn();
        }
        else {
            // Notification if you try to loan more than accepted.
            accepted = balance * 2;
            alert(`You can only take a loan at ${accepted}`)
        }
    }
    else {
        // Alert if you already have a loan
        alert(`You have to many loans`);
    }
}

// Increment salary //
function salaryFunction(){
        salary+=100
        salaryBalElement.innerHTML = salary;
}

//Payback the loan function from salary
function payback(){
    loanAmount = loanAmount - salary;
    salary = 0;
    if(loanAmount < 0){
        salary = -loanAmount
        loanAmount = 0;
    }
    currentLoanElement.innerHTML = loanAmount;
    salaryBalElement.innerHTML = salary;
    paybackBtn()
}
// Manipulating style(Button) to show element depending on taken loan or not.
const paybackBtn = () => {
    if (loanAmount > 0) {
        payBackElement.style.display = "block"
    } else {
        payBackElement.style.display = "none"
    }}
    
const banking = () => {
    // If there is no loan and salary is greater than 0 - Bank it to balance. Then reset salary.
    if (loanAmount <= 0 && salary > 0) {
        balance = parseInt(salary) + parseInt(balance);
        balanceElement.innerHTML = balance;
        salary = 0;
        salaryBalElement.innerHTML = salary;
    // If there is a loan. Start buy decreasing the loan by 10% (0.1)
    // Rest of the salary (90% - 0.9) will be banked.
    // Update frontend and salary variable.
    } else if (loanAmount > 0) {
        loanAmount = loanAmount - parseInt(salary*0.1)
        currentLoanElement.innerHTML = loanAmount;
        balance = parseInt(balance + salary*0.9)
        balanceElement.innerHTML = balance;
        salary = 0;
        salaryBalElement.innerHTML = salary;
    }

}

// Laptops / Computers
// Fetchin data and store it to computers
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputers(computers));

    // Loop all data from API.
const addComputers = (computer) => {
    console.log(computer)
    computers.forEach(x => addComputer(x));
    computerImgElement.src = `https://noroff-komputer-store-api.herokuapp.com/assets/images/1.png`;
    // Display all first data
    featureElement.innerHTML = computers[0].specs;
    computerTitleElement.innerHTML = computers[0].title;
    computerInfoElement.innerHTML = computers[0].description;
    computerPriceElement.innerHTML = computers[0].price;
}

// Add the computers to options scroll bar
const addComputer = (computer) => {
    let computerElement = document.createElement("option");
    computerElement.value = computer.id;
    console.log(computerElement.value)
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement)
} 

// Display computer features for selected option
const computerChange = () => {
    featureElement.innerHTML = '';
    selectedComputer = computers[computersElement.selectedIndex];
    selectedComputer.specs.forEach(specification => {
        let computerFeature = document.createElement("li")
        computerFeature.innerHTML = specification;
        featureElement.appendChild(computerFeature);      
    })
    // Invoke the functions on row 142 & 148.
    computerImage(selectedComputer)
    computerInfo(selectedComputer)
}

// Display image for the selected computer
const computerImage = (img) => {
    const image = `https://noroff-komputer-store-api.herokuapp.com/${img.image}`;
    computerImgElement.src = image;
}

// Display infomation about the selected computer
 const computerInfo = (selectedComputer) => {
    computerTitleElement.innerHTML = selectedComputer.title;
    computerInfoElement.innerHTML = selectedComputer.description;
    computerPriceElement.innerHTML = selectedComputer.price;
} 

// Buy button function
const buyBtn = () => {
    const selectedPrice = computers[computersElement.selectedIndex];
    let laptopPrice = selectedPrice.price;
    // Controll if your balance is equal or greater than the price for the laptop.
    if (balance >= laptopPrice) {
        alert("You have purchased the computer!")
        // Lower your balance when the purchase is done
        balance = parseInt(balance) - parseInt(laptopPrice);
        balanceElement.innerHTML = balance;
    } else {
        alert("You dont have enought balance on you'r account.")
    }
}

// List of Event listeners
takeLoanElement.addEventListener("click", takeLoan)
listedComputersElement.addEventListener("change", computerChange);
salaryButtonElement.addEventListener("click", salaryFunction);
payBackElement.addEventListener("click", payback);
bankingElement.addEventListener("click", banking);
buyBtnElement.addEventListener("click", buyBtn);

